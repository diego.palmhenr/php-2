create database ejemplo;

use ejemplo;

create table registros (
rut varchar (9),
nombres varchar(100),
apellidos varchar(100),
fecha date,
edad int,
email varchar(100),
region varchar(100),
provincia varchar(100),
comuna varchar(100)
);
